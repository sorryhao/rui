\section{Ad Network Identification}\label{sec:detect}
Applications that monetize with advertisements partner with ad networks and
embed code called ad libraries from them to display and manage advertisements.
Our goal was to comprehensively identify ad networks that participate in the
Android ecosystem. Some simple domain knowledge, such as which ad networks are
there in the market, may not provide a comprehensive list. We instead resorted
to two systematic approaches to do this identification based on the ad libraries
embedded in the code.


\paragraphx{Approach 1}
We exploit the fact that one ad network will likely be used by many applications
and thus common ad library code will be found in all applications using an ad
network. The native programming platform for Android applications is Java and
Java packages provide mechanisms to organize related code in namespaces. Ad
libraries themselves have packages that can serve as their identifying
signatures.

In our first approach, we collected packages from all applications in our
dataset and created a package hierarchy together with the frequency of
occurrence of each package. We sorted the packages and then manually searched
the most frequent packages to identify ad libraries. For example, after sorting,
packages such as \texttt{com.facebook} and \texttt{com.google.ads} appear at the
top. Then we identified the nature of each package, i.e., whether it
constituted an ad library, based on either prior knowledge or manually searching
information about that package on the Web.

%The whole process could be aided in several ways. We wrote a small GUI tool to
%aid the search in several ways. It keeps the package hierarchy, showing packages
%sorted by frequency, allowing us to select any package to annotate it. Once a
%package is annotated, the selection automatically moves to the next package.
%Wildcard annotations that annotate an entire package subtree (e.g.,
%\texttt{com.twitter}) are also possible.

\paragraphx{Approach 2}
The previous approach became cumbersome when we reached frequencies of a few
hundred because many non-ad packages also had such frequencies. Our alternative
approach allows for comprehensive identification of ad libraries without
depending on the frequency of occurrence of those ad libraries. Our approach
relies on the fact that the main application functionality is only loosely
coupled with the functionality of ad libraries. Thus, we use the technique
described by Zhou et al.~\cite{zzg+13} to detect loosely coupled components in
the applications. The coupling is actually measured in terms of characteristics
such as field references, method references, and class inheritances across classes.  Ideally,
all the packages of one ad library will be grouped into one component. In
reality, this does not always happen and it may also happen that classes that
should have been in different components end up in same components. However, the
errors are tolerable and can be manually analyzed.

The manual analysis is further eased by employing a clustering technique
described as follows.  We create a set of Android APIs called in an application
component. This set of APIs forms a signature for the component. We map these
APIs to integers to enable efficient set computations. Based on this, ad library
instances with the same version have matching API sets. For different versions,
the sets will be similar but not identical. We run this analysis on components
extracted from all applications and then use the Jaccard distance to compute
dissimilarity between API sets. If it is below a certain threshold (we used
0.2), we place the components in the same cluster.
Thus packages of different ad libraries end up in different clusters, and then
clusters can be easily mapped to ad libraries.

%The decoupling is very important, we group packages into modules according to their associations categorized in
%field references, method references, class inheritances, package homogeny. We create a graph to implements this,
%and each vertex represents a package. The weights on the edges denote how closely the associated packages are
%related to each other. Once the graph is set down, we can merge the vertices which are associated by an edge with
%weight large enough. To do this quickly we use union-find algorithm. In order to make decoupling work better, we
%tune the parameters according to the 90 ad libraries we have already had.

\subsection{Results}
Using the two approaches, we were able to identify 201 ad networks
in our dataset. Some ad networks have ad libraries with several package names. For
example, \texttt{com.vpon.adon} and \texttt{com.vpadn} belong to the same network.
We combine such instances together to be represented as ad network for later
measurements. More notably, Google's Admob and DoubleClick platforms are both
represented as Google ads.
%We also noticed that presently Google ads is
%integrated with the Google Play Services SDK. Thus, if this SDK is included for
%any service other than advertisements, we may get a false positive for Google
%ads. We therefore do a more specific search in the AndroidManifest.xml (this
%file is present in every Android application) for artifacts necessary for
%working Google ads. We now list some interesting findings we made while
%discovering these ad networks.

%\paragraph{Obfuscated ad libraries}
Our approach to use package names to identify ad libraries is contingent upon
the assumption that ad library packages are not obfuscated. This is true for
most cases that we know of: the top-level packages work quite well to identify
most ad libraries. However, Airpush is one known ad network that obfuscates its ad
libraries such that they are no longer identifiable with package
names~\cite{airpushobfuscation}. While applying our second approach, which is
immune to lexicographic obfuscations, we also detected obfuscated Airpush packages, all
ending up in a few clusters. The clusters have the non-obfuscated package
com.airpush.android as well as obfuscated ones like com.cRDpXgdA.kHmZYqsQ70374
and com.enVVWAar.CJxTGNEL99769.


%\paragraph{Prevalance}
In all we detected ad libraries in 53\% of applications in the Google Play
dataset and 50\% of Chinese stores applications.
A large number of applications do not contain ad
libraries. Some possible sources of revenue include a paid premium version of
application available separately and in-application purchases. We also believe
that a large number of non-ad-supported applications do not create any revenue
for the developers. Studying sources of revenue for non-ad supported
applications is beyond the scope of this work.

%\paragraph{Demographics}
%We found some ad libraries to target a particular demographics such as people in
%certain countries and geographic locations. For example, we detected several
%Chinese ad networks such as Umeng, Youmi and Tencent ads; Japanese networks such
%as Adlantis and Microad; and Korean networks such as Sktelecom ads and Cauly.

%\paragraph{Non-traditional networks}
%There are some non-traditional networks also that we found. These networks do
%not serve advertisements but do provide monetization opportunities to
%developers. For example, Pollfish is a survey platform: developers can embed the
%Pollfish SDK in their applications and Pollfish provides the application users
%incentives to complete a few surveys. The developers are paid for each completed
%survey. Another example is PocketChange, which provides a virtual currency and
%ways for users to earn and spend the currency while providing opportunities for
%developers for greater user retention and for advertisers to present their
%product PocketChange's virtual store. Kiip and SessionM also offer similar
%functionality.  Such platforms are not strictly ad networks but for the sake of
%this study are considered ad networks.

