\section{Permission Overprivilege}\label{sec:reputation}
In Android, a number of system APIs and access to other functionality are
protected by permissions, which an application is granted at installation time.
During a normal application installation, users are able to see these
permissions as short English descriptions what an application is allowed to.
Permissions thus play an important role in preserving the security and privacy
of the user.  Ad libraries use system APIs to perform their function and may
need permissions to use some APIs. If a permission is not needed for the primary
functionality of the application but is only needed to support ad networks, a
wary user may not want to use such an application. In the context of this paper,
we call such applications as {\em overprivileged}.
Our goal here is to understand the prevalence of overprivileged applications
that request permissions only to show additional advertisements and whether the
users are actually repelled by such behavior.

We developed a tool to perform this measurement. At a lower level, we would like
to identify which permissions are needed for which components of the
application. For this, our tool disassembles the application code and then examines
each class (recall that Android applications are programmed in Java) to find if
any API calls within it require a permission. Researchers have earlier studied
API-to-permission mapping on Android~\cite{fch+11,azhl12}. We used the PScout
dataset~\cite{azhl12} for API-to-permission mapping. Apart from APIs,
permissions are also required for accessing content providers (which provide a
database-like interface) and sending and receiving intents, which are used for
inter-process communication in Android. The use of these functionalities can be
detected through the presence of certain strings or field-accesses. The mappings
from such strings and fields is also provided in the PScout dataset. The details
of our tool are similar to those described by Felt et al.~\cite{fch+11}. API
calls may also be made with reflection. In general, we do not support reflection
analysis but do examine strings in a method that uses reflection API to consider
the possibility if they could be used for reflection. It is also worth noting
that while API calls are easy to attribute to the classes that contain them,
strings may sometimes be stored in XML resources accompanying the application
code. Attributing XML resources to code components using them appears to be a
challenging problem without an easy solution. Our tool therefore is limited in
this aspect.

Our tool is written in less than 300 lines of Python and uses \texttt{dexdump},
which comes with the official Android SDK, for application code disassembly.
\texttt{Dexdump} itself is written in C and we found it to be faster than other
free disassemblers for Android. Our tool takes merely 1.3 seconds on average to
process an Android application on a Xeon processor using a single thread of
execution. The application analysis can thus be scaled easily to hundreds of
thousands of applications.

Ad networks often have optional functionality, which would be used only if the
relevant permissions are available. For example, some ad networks provide
geo-targeted advertisements if the permissions of accessing device location are
available. Our tool detects such functionality and possibly need of the
related permissions. However, for the purpose of the present study, we
additionally compare the possible permissions with the permissions actually
requested in application manifest and for each ad library, keep only the
permissions that are also requested. Finally, we compare such permission sets of
ad libraries with the permissions needed for rest of the application code and
thus identify any overprivilege. Throughout our analysis, we omit the INTERNET
permission, which allows network access to outside the device, as this
permission is necessary to serve any advertisements and so does not provide any
additional insights. The rest of this section now describes our results.

\paragraph{Overprivilege prevalence}
The measurements here answer the question how prevalent are over-privileged
applications. Figure~\ref{fig:overapp} presents the variation of percentage of
applications with respect to all ad-supported applications against the number of
permissions needed only to support advertisements. We note that a good majority
of applications on Google Play (69\%) do not request additional permissions for
advertisements. The rest do but their number quickly decreases with the increase
in the number of additional permissions. We have confirmed this to be an
exponential decay. The same figure also presents measurements for the
Chinese stores, a substantially lower 54\% applications are not overprivileged.
We see a consistent, marked trend of greater percentage of applications with
additional ad-only permissions when compared to Google Play.

Figure~\ref{fig:overperm} further examines prevalence by plotting the frequency of
the most frequently requested ad-only permissions. After the INTERNET
permission, which we omit from the analysis, The ACCESS\_NETWORK\_STATE
permission is the most frequently requested permission and is required by Google
ads, the most dominant ad network. ACCESS\_WIFI\_STATE is permission required
for many ad networks. The READ\_PHONE\_STATE permission allows access to the
device identifiers such as IMEI and phone number, which ad libraries use as
device-wide cookies. This permission thus protects access to moderately private
data. The next permission, WRITE\_EXTERNAL\_STORAGE, allows the requester to
write to device's SD card. The next two permissions protect access to user's
location, which ad networks use of location-based targeting of advertisements.
The location permissions, which are considered privacy sensitive, are optional
for most ad networks, however some developers still choose to include this
permission for advertisements only, apparently to improve revenue. The plot
depicts several other permissions that have important security or privacy
implications, or the abuse of which (such as that of the VIBRATE permission) can
be quite annoying.

Figure~\ref{fig:overperm} reiterates our discovery the frequency of overprivilege on
Chinese stores is significantly more than on Google Play. In fact, the ad-only
requests for location permissions are three times as high as on Google Play
whereas other invasive permissions, such as INSTALL\_SHORTCUT (which installs
shortcuts on home screen), VIBRATE, and READ/WRITE\_HISTORY\_BOOKMARKS are also
requested several times more than on Google Play. One reason of this could be
stronger policing on Google Play and enforcement of Play's terms and conditions:
Google has been known to remove low-quality and aggressive applications
\cite{googleremoves1,googleremoves2}. Another reason could also be a
sociocultural aspect: possibly people in China do not complain as much over
application's invasiveness as North Americans do.

\paragraph{User acceptance}
We now examine whether the users accept applications overprivileged due to
advertisements. Figure~\ref{fig:overavgdl} shows the average number of downloads
per application as the number of ad-only permissions increases. The average
downloads are very similar for lower number of overprivilege. However, as the
number of additional ad-only permissions increases the average number of
downloads decreases. We believe this is due to the fact that an overprivilege
of just one or two permissions is due to minor permissions such as
ACCESS\_NETWORK\_STATE or ACCESS\_WIFI\_STATE, which neither affect user
experience nor invade into user privacy. It is actually the agressive ad
networks that request many more permissions. In some cases the users may not
even be aware of the permissions requested and their security and privacy
implications~\cite{fhe+12}, however the aggressiveness of some ad networks may be
sufficient to turn them away from the applications hosting these networks.
Another intuition is that developers providing quality functionality and content
in applications may themselves move away from aggressive networks or may be
more careful of not declaring permissions without necessity. Further studies are
needed to deepen our understanding of dynamics around the interaction of user,
developer, and the ad network.

